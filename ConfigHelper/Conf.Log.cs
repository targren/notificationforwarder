﻿namespace Notification_Forwarder.ConfigHelper
{
    public partial class Conf
    {        
        public static void Log(string content, LogLevel level = LogLevel.Info, bool ignoreNewLine = true)
        {
            
            Logs.Add(new LogEntry(ignoreNewLine ? content.Replace("\r", "").Replace("\n", " ") : content, level));
            if (Logs.Count > 25) //Purge logs after 25 entries to save memory and stop the stupid OS from suspending the app
            {
                Logs.RemoveAt(0);
            }
        }
    }
}
