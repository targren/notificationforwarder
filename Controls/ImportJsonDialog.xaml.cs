﻿using ColorCode;
using System;
using System.Threading.Tasks;
using Windows.ApplicationModel.DataTransfer;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Documents;
using Newtonsoft.Json;

namespace Notification_Forwarder.Controls
{
    public sealed partial class ImportJsonDialog : ContentDialog
    {
        public ImportJsonDialog() => InitializeComponent();
       
        public string JSONContent { get; set; }

        public static async Task Open()
        {
            var dialog = new ImportJsonDialog();
            //var formatter = new RichTextBlockFormatter();
            //formatter.FormatRichTextBlock(content, Languages.JavaScript, dialog.TextBox_DumpContent);
            _ = await dialog.ShowAsync();
        }

        private async void ContentDialog_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {

            Windows.UI.Popups.MessageDialog dlg = new Windows.UI.Popups.MessageDialog("Settings Imported", "Import OK");
            try
            {
                ConfigHelper.Conf Parsed = JsonConvert.DeserializeObject<ConfigHelper.Conf>(JSONContent);
                ConfigHelper.Conf.Save(Parsed);
            }
            catch (Exception Ex)
            {
                dlg.Content = "Error Importing JSON: \n" + Ex.Message;
                dlg.Title = "Failure";
                args.Cancel = true;
            }

            await dlg.ShowAsync();
        }

        private void TextBox_JSON_Content_TextChanged(object sender, TextChangedEventArgs e)
        {
            JSONContent = TextBox_JSON_Content.Text;          
        }
    }
}
